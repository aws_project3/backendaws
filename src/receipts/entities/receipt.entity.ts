import { Employee } from 'src/employees/entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
// import { Product } from 'src/products/entities/product.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  total_price: number;

  @Column({ type: 'float' })
  discount: number;

  @Column({ type: 'float' })
  cash: number;

  @Column({ type: 'float' })
  change: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.receipt)
  employee: Employee;

  @OneToOne(() => Order, (order) => order.receipt)
  @JoinColumn()
  order: Order;
}
