import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreateReceiptDto {
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  total_price: number;

  @IsNotEmpty()
  @IsNumber()
  discount: number;

  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  cash: number;

  @IsNotEmpty()
  @IsNumber()
  change: number;

  employeeId: number;

  @IsNotEmpty()
  orderId: string;
}
