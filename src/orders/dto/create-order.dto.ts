import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateOrderDto {
  @IsNotEmpty()
  table_number: number;
  orderItems: CreateOrderItemDto[];
  employeeId: number;
}

export class CreateOrderItemDto {
  @IsString()
  name: string;

  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  @IsNumber()
  qty: number;
}
